const service = require("./service")

const showCart = async (req, res) => {
    try { 
        const userID = req.params.id
        const serviceRes = await service.showCart(userID)
        res.json(serviceRes)
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

const addOrder = async (req, res) => {
    try { 
        const userID = req.body.userID
        const itemID = req.body.itemID
        const count = req.body.count

        await service.addOrder(userID, itemID, count)

        res.json('ok')
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

const getByID = async (req, res) => {
    try { 
        const id = req.params.id
        const serviceRes = await service.getByID(id)
        res.json(serviceRes)
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

const deleteCart = async (req, res) => {
    try { 
        const userID = req.params.id
        const serviceRes = await service.deleteCart(userID)
        if(serviceRes > 0) {
            res.json('clear order in cart success')
        } else {
            res.json('no order in cart')
        }
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

const updateCart = async (req, res) => {
    try { 
        const userID = req.params.id
        const itemID = req.body.itemID
        const count = req.body.count
        await service.updateCart(userID, itemID, count)
        res.json('ok')
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

module.exports = {
    showCart,
    addOrder,
    getByID,
    deleteCart,
    updateCart
};