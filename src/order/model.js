const { DataTypes, Model } = require('sequelize');
const connectDB = require('../connection/sequelize')


const sequelize = connectDB.sequelize()
class Order extends Model {}

Order.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: true,
  },
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  item: {
    type: DataTypes.ARRAY(DataTypes.INTEGER),
    allowNull: false,
  },
  status: {
    type: DataTypes.ENUM,
    values: ['PENDING', 'PURCHASED'],
    defaultValue: 'PENDING',
  },
  count: {
    type: DataTypes.ARRAY(DataTypes.INTEGER),
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
  },
  updatedAt: {
    type: DataTypes.DATE,
  },
}, {
  sequelize,
  timestamps: true,
  modelName: 'order',
  createdAt: true,
  updatedAt: true,
});

module.exports = Order
