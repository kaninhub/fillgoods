const Order = require('./model')
const config = require('../config')
const connectDB = require('../connection/sequelize')
const productService = require('../product/service')
const { Op } = require('sequelize');

const sequelize = connectDB.sequelize()

const addOrder = async (userID, itemID, count) => {
    try {
        // Minimum quantity are 0 and maximum quantity are 99
        if(count < 0 || count > 99) {
            throw Error("Minimum quantity are 0 and maximum quantity are " + config.maxQuatity)
        }
        const order = await Order.findOne({
            where: { 
                user_id: userID,
                status: "PENDING",
            }
        })
        if (order) {
            // if add order that exist cart
            const checkExistCart = order.item.indexOf(itemID)
            if (checkExistCart !== -1) {
                // add count
                order.count[checkExistCart] = order.count[checkExistCart] + count
                await Order.update({
                    count: order.count,
                }, {
                    where: {
                        user_id: userID,
                        status: "PENDING",
                    }
                })
            } else {
                await order.update({
                    item: sequelize.fn('array_append', sequelize.col('item'), itemID),
                    count: sequelize.fn('array_append', sequelize.col('count'), count),
                }, {
                    where: {
                        user_id: userID,
                        status: "PENDING",
                    }
                })
            }
        } else {
            await Order.create({
                user_id: userID,
                item: [itemID],
                count: [count],
            })
        }
    } catch (err) {
        throw Error(err)
    }
}

const showCart = async (userID) => {
    try {
        const data = await Order.findOne({
            where: {
                user_id: userID,
                status: "PENDING",
            },
            raw: true,
            attributes: ['item', 'count']
        })
        if (!data) {
            return {}
        }

        const items = data.item
        const counts = data.count
        const productData = await productService.getList(items)

        let detailCart = []
        let totalOrderCost = 0
        for(let i = 0; i < items.length; i++) {
            const product = productData.find(productelement => productelement.id === items[i])
            const count = counts[i]
            console.log("product", product)
            const totalThisProduct = product.price * counts[i]
            detailCart.push({
                id: product.id,
                name: product.name,
                count,
                totalThisProduct,
            })
            totalOrderCost = totalOrderCost + totalThisProduct
        }
        return {
            detailCart,
            totalOrderCost,
        }
    } catch (err) {
        throw Error(err)
    }
}

const getByID = async (id = 1) => {
    try {
        const data = await Order.findByPk(id);
        return data
    } catch (err) {
        throw Error(err)
    }
}

const deleteCart = async (userID) => {
    try {
        const delateData = await Order.destroy({
            where: { 
                user_id: userID,
                status: "PENDING",
            }
        })
        return delateData
    } catch (err) {
        throw Error(err)
    }
}

const updateCart = async (userID, itemID, newCount) => {
    try {
        const data = await Order.findOne({
            where: { 
                user_id: userID,
                status: "PENDING",
            },
            raw: true
        })
        // find itemID
        const findItemIndex = data.item.indexOf(itemID)
        const arrayCount = data.count
        const arrayItem = data.item

        if(findItemIndex === -1) {
            throw Error('not found itemID: ' + itemID)
        }
        // update newCount 
        if(newCount === 0) {
            // have to slice index out
            const newAryItem = arrayItem.slice(0, findItemIndex).concat(arrayItem.slice(findItemIndex + 1, arrayItem.length))
            const newAryCount = arrayCount.slice(0, findItemIndex).concat(arrayCount.slice(findItemIndex + 1, arrayCount.length))
            await Order.update({
                item: newAryItem,
                count: newAryCount,
            }, {
                where: {
                    user_id: userID,
                    status: "PENDING",
                }
            })
        } else {
            // replace index with new count
            arrayCount[findItemIndex] = newCount
            await Order.update({
                count: arrayCount,
            }, {
                where: {
                    user_id: userID,
                    status: "PENDING",
                }
            })
        }
    } catch (err) {
        throw Error(err)
    }
}

module.exports = {
    addOrder,
    showCart,
    getByID,
    deleteCart,
    updateCart,
};