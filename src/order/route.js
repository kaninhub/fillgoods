const express = require('express')
const router = express.Router()
const controller = require('./controller');
const cors = require('cors');

router.get('/user-id/:id', controller.showCart)
router.post('/', controller.addOrder)
router.get('/id/:id', controller.getByID)
router.delete('/user-id/:id', controller.deleteCart)
router.put('/user-id/:id', controller.updateCart)


module.exports = router