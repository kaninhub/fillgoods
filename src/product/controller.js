const service = require("./service")

const getAll = async (req, res) => {
    try { 
        const limit = req.query.limit
        const offset = req.query.offset

        const serviceRes = await service.getAll(limit, offset)

        res.json(serviceRes)
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

const create = async (req, res) => {
    try { 
        const name = req.body.name
        const description = req.body.description
        const price = req.body.price

        const serviceRes = await service.create(name, description, price)

        res.json(serviceRes)
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

const getByID = async (req, res) => {
    try { 
        const id = req.params.id

        const serviceRes = await service.getByID(id)

        res.json(serviceRes)
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

module.exports = {
    getAll,
    create,
    getByID,
};