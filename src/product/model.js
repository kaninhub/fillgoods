const { DataTypes, Model } = require('sequelize');
const connectDB = require('../connection/sequelize')

const sequelize = connectDB.sequelize()
class Product extends Model {}

Product.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.STRING,
  },
  price: {
    type: DataTypes.DOUBLE,
  },
  createdAt: {
    type: DataTypes.DATE,
  },
  updatedAt: {
    type: DataTypes.DATE,
  },
}, {
  sequelize,
  timestamps: true,
  modelName: 'product',
  createdAt: true,
  updatedAt: true
});

module.exports = Product