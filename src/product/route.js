const express = require('express')
const router = express.Router()
const controller = require('./controller');
const cors = require('cors');

router.get('/', controller.getAll)
router.post('/', controller.create)
router.get('/id/:id', controller.getByID)

module.exports = router