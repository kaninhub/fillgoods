const Product = require("./model")
const { Op } = require('sequelize');


const create = async (name, description, price) => {
    try {
        const product = await Product.create({
            name,
            description,
            price,
        })
        const responseDB = product.toJSON()
        return responseDB
    } catch (err) {
        throw Error(err)
    }
}

const getAll = async (limit = 10, offset = 0) => {
    try {
        const data = await Product.findAll({
            limit,
            offset,
            where: { },
            raw: true,
        });
        return data
    } catch (err) {
        throw Error(err)
    }
}

const getByID = async (id = 1) => {
    try {
        const data = await Product.findByPk(id);
        return data
    } catch (err) {
        throw Error(err)
    }
}

const getList = async (list) => {
    try {
        console.log(list)
        const data = await Product.findAll({
            where: {
                id: {[Op.in]: list},
            },
            raw: true,
        });
        return data
    } catch (err) {
        throw Error(err)
    }
}

module.exports = {
    create,
    getAll,
    getByID,
    getList,
};