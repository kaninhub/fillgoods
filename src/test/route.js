const express = require('express')
const router = express.Router()
const controller = require('./controller');
const cors = require('cors');

router.get('/health', controller.health)
router.post('/test-db', controller.testDB)

module.exports = router