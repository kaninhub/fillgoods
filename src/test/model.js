const { DataTypes, Model } = require('sequelize');
const connectDB = require('../connection/sequelize')


const sequelize = connectDB.sequelize()
class Test extends Model {}

Test.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: true,
  },
  value: {
    type: DataTypes.STRING
  },
  createdAt: {
    type: DataTypes.DATE,
  },
  updatedAt: {
    type: DataTypes.DATE,
  },
}, {
  sequelize,
  timestamps: true,
  modelName: 'test',
  createdAt: true,
  updatedAt: true
});

module.exports = Test