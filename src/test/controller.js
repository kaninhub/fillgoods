const service = require("./service")

const health = async (req, res) => {
    try { 
        const response = {
            health: "ok"
        }
        res.json(response)
    } catch(err) {
        res.status(500).send(err.toString())
    }
};

const testDB = async (req, res) => {
    try { 
        const value = req.body.value
        const serviceRes = await service.testDB(value)

        res.json(serviceRes)
    } catch(err) {
        res.status(500).send(err.toString())
    }
};


module.exports = {
    health,
    testDB,
};