const Test = require("./model")


const testDB = async (value) => {
    try {
        const test = await Test.create({
            value,
        })
        const responseDB = test.toJSON()
        return responseDB
    } catch (err) {
        throw Error(err)
    }
}

module.exports = {
    testDB,
};