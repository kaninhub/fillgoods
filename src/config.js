const port = process.env.PORT || 8888
const databaseUrl = process.env.DATABASE_URL || 'postgres://postgres:postgres@localhost:5432'
const maxQuatity = 99

module.exports = {
    port,
    databaseUrl,
    maxQuatity,
};