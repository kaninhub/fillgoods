const bodyParser = require('body-parser')
const cors = require('cors');
const express = require('express')
const path = require('path')
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const config = require('./config')
const connectDB = require('./connection/sequelize')
const orderRouter = require('./order/route')
const productRouter = require('./product/route')
const testRouter = require('./test/route')

const app = express()
const swaggerDocument = YAML.load(path.join(__dirname, './swagger.yaml'));
// connect db
connectDB.sequelize()

app.use(bodyParser.json())
app.use(cors({
    origin: '*',
}));

//router
app.use('/order', orderRouter)
app.use('/product', productRouter)
app.use('/test', testRouter)

// for swagger
// app.use('/api-docs', cors(), swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(config.port, () => {
    console.log('server running on port: ', config.port)
})
