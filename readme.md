# STEP RUN FILLGOODS

## Clone
```
$ git clone https://gitlab.com/kaninhub/fillgoods.git
$ cd fillgoods
```

## Install all dependencies
```
$ npm install
```

## Run migrates
```
$ node node_modules/db-migrate/bin/db-migrate up
```

## Setup env

make port **8888** avialiable and 
postgres url: **postgres://postgres:postgres@localhost:5432**


## Run server
```
$ npm start
```

## API POSTMAN TEST

is **.\fillgoods.postman_collection.json** can import and test

