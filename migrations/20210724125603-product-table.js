'use strict';

exports.up = function (db, callback) {
  db.createTable('products', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    name: 'string',
    description: 'string',
    price: 'real',
    createdAt: 'date',
    updatedAt: 'date',
  }, callback);
};