'use strict';

exports.up = async (db, callback) => {

  await db.insert('users', 
  ['id', 'name', 'gender', 'age'],
  ['1', 'john', 'male', 43],
  callback);

  await db.insert('users', 
  ['id', 'name', 'gender', 'age'],
  ['2', 'robin', 'male', 15],
  callback);

  await db.insert('users', 
  ['id', 'name', 'gender', 'age'],
  ['3', 'Sara', 'female', 28],
  callback);

  await db.insert('users', 
  ['id', 'name', 'gender', 'age'],
  ['4', 'john', 'male', 31],
  callback);

  await db.insert('users', 
  ['id', 'name', 'gender', 'age'],
  ['5', 'edward', 'male', 19],
  callback);

};