'use strict';

exports.up = function (db, callback) {
  db.createTable('tests', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    value: 'string',
    createdAt: 'date',
    updatedAt: 'date',
  }, callback);
};