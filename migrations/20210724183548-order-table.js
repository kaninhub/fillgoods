'use strict';

exports.up = function (db, callback) {
  db.createTable('orders', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    user_id: 'int',
    item: 'int[]',
    status: 'string',
    count: 'int[]',
    createdAt: 'date',
    updatedAt: 'date',
  }, callback);
};