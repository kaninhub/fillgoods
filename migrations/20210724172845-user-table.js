'use strict';

exports.up = function (db, callback) {
  db.createTable('users', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    name: 'string',
    gender: 'string',
    age: 'int',
    createdAt: 'date',
    updatedAt: 'date',
  }, callback);
};